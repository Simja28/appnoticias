import { RespuestaTopHeadLines } from './../app/pages/interfaces/interfaces';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

const headers = new HttpHeaders({
  'X-Api-Key': apiKey,
});

@Injectable({
  providedIn: 'root',
})
export class NoticiasService {
  constructor(private http: HttpClient) {}

  getTopHeadlines() {
    return this.executeQuery<RespuestaTopHeadLines>(`top-headlines?country=mx`);
  }

  getTopHeadlinesCategory(category: string) {
    return this.executeQuery<RespuestaTopHeadLines>(`top-headlines?country=mx&category=${category}`);
  }

  private executeQuery<T>(query: string) {
    query = apiUrl + query;
    return this.http.get<T>(query,{headers});
  }
}
