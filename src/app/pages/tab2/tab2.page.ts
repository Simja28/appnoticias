import { NoticiasService } from './../../../services/noticias.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { Article } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  @ViewChild(IonSegment) segment: IonSegment;

  categorias = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology'];
  noticias: Article[] = [];

  constructor(private service: NoticiasService) {
  }

  ngOnInit()
  {

  }

  ngAfterViewInit(){
    console.log(this.segment);
    this.segment.value = this.categorias[0];
    this.cargarNoticias(this.cargarNoticias[0]);
  }

  changeCategory(event)
  {
    console.log(event);
    this.noticias = [];
    this.cargarNoticias(event.detail.value);
  }

  cargarNoticias(categoria: string){
    this.service.getTopHeadlinesCategory(categoria).subscribe(
      resp => {
        this.noticias.push(...resp.articles)
      }
    );
  }

}
