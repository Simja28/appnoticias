import { Article } from './../interfaces/interfaces';
import { NoticiasService } from './../../../services/noticias.service';
import { Component } from '@angular/core';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  noticias: Article [] = [];

  constructor(private noticiasServ: NoticiasService) {
    this.noticiasServ.getTopHeadlines().subscribe(
      res=>{
        console.log(res.articles);
        this.noticias.push(...res.articles);
      }
    );
  }
}
